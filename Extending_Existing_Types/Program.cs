﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extending_Existing_Types
{
    class Program
    {
        static void Main(string[] args)
        {
            Some_Class temp = new Some_Class();

            Debug.WriteLine("value of hello there is - " + temp.do_stuff());
        }
    }

    //this is the original class/type
    //I want to extend this type but I dont wish to add the code here in the type definition
    class Some_Class
    {
        public int hello_there { get; set; }

        public Some_Class()
        {
            this.hello_there = 10;
        }
    }

    //this class is extending Some_Class
    static class Extending_Some_Class
    {
        //this method now belongs to Some_Class becuase of the 'this' keyword
        //if we had not used the this keyword, we would simply be using a parameter of type Some_Class
        //in which case, do_stuff would only be available for instances of Extending_Some_Class
        //now, thanks to the usage of this, the extension is done.
        //that means, objects created using Some_Class will also have access to this method
        public static int do_stuff(this Some_Class some_class)
        {
            return some_class.hello_there;
        }
    }


}
